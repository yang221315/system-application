# 系统应用

#### 介绍
基于c#桌面应用和sql server开发的系统应用

#### 软件架构
软件架构说明


#### 安装教程
如果电脑安装了vs2019和sql server相关环境之后，相关下载解压之后，将data保存在数据库中，注意修改数据库路径和密码，保持一致。
修改完成之后，就可以运行，直接点击exe文件。

#### 使用说明

本系统并不复杂，自行摸索即可！
提示：本系统和普通Windows桌面应用使用方法一致。三种角色（student，teacher，manager）
可以注册账号，可以找回密码。

#### 运行效果
![输入图片说明](https://images.gitee.com/uploads/images/2021/1108/221203_7ab57ebc_9060262.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/1108/221234_81960cbb_9060262.png "屏幕截图.png")
