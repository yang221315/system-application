﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 系统应用开发
{
    class Sql
    {
        static public string ConnString = "server=杨磊;database=c#数据库;uid=杨磊;pwd=2001221315Lei";
        public SqlConnection conn;

        //其他操作
        #region 增加、修改、删除模块
        public int NonQuery(string sql, SqlParameter[] parameters = null)
        {
            conn = new SqlConnection(ConnString);
            int result = 0;
            try
            {
                conn.Open();  
                SqlCommand cmd = new SqlCommand(sql, conn);
                if(parameters!=null)
                {
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddRange(parameters);
                    result = cmd.ExecuteNonQuery();
                }
            }
            catch
            {
                Console.WriteLine("连接出错！");
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();   
                }
            }
            return result;
        }
        #endregion

        //查询操作
        #region 查询模块
        public DataSet Query(string sql)
        {
            conn = new SqlConnection(ConnString);
            DataSet ds = new DataSet();
            try
            {
                conn.Open();       
                SqlDataAdapter adp = new SqlDataAdapter(sql, conn);
                adp.Fill(ds, "table1");
            }
            catch
            {
                Console.WriteLine("打开失败！");
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }
            return ds;
        }
        #endregion

        //登录操作
        #region 登录模块
        public int Login(string sql, SqlParameter[] parameters)
        {
            int flag = 0;
            conn = new SqlConnection(ConnString);

            try
            {
                conn.Open();
                SqlCommand sqlCommand = new SqlCommand(sql, conn);
                if(parameters!=null)
                {
                    sqlCommand.Parameters.Clear();
                    sqlCommand.Parameters.AddRange(parameters);
                    SqlDataReader reader = sqlCommand.ExecuteReader();
                    if (reader.Read())
                    {
                        flag = 1;
                    }
                }
            }
            catch
            {
                Console.WriteLine("打开失败！");
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }
            return flag;
        }
        #endregion
    }
}
