﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 系统应用开发
{
    public partial class 管理员找回密码 : Form
    {
        public static String BianHao;
        public static String MiMa;

        Sql sql = new Sql();
        public 管理员找回密码()
        {
            InitializeComponent();

            String startupPath = Application.StartupPath;
            startupPath = startupPath.Substring(0, startupPath.LastIndexOf("\\"));
            string path = startupPath.Substring(0, startupPath.LastIndexOf("\\"));

            this.BackgroundImage = Image.FromFile(path + @"\\图片素材\\找回背景3.jpg");
            this.BackgroundImageLayout = ImageLayout.Stretch;
            //位置屏幕居中
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
        }

        //提交查找
        private void button1_Click(object sender, EventArgs e)
        {
            int flag = 0, flag1 = 0, flag2 = 0;
            int record = 0, record1 = 0, record2 = 0;

            String s = "select * from [管理员表]";
            DataSet dss = new DataSet();
            dss = sql.Query(s);
            DataTable data = dss.Tables["table1"];

            for (int i = 0; i < data.Rows.Count; i++)
            {
                if (textBox4.Text.Trim() == data.Rows[i]["姓名"].ToString().Trim())
                {
                    flag = 1;
                    record = i;
                }
            }

            for (int i = 0; i < data.Rows.Count; i++)
            {
                if (textBox5.Text.Trim() == data.Rows[i]["身份证"].ToString().Trim())
                {
                    flag1 = 1;
                    record1 = i;
                }
            }

            for (int i = 0; i < data.Rows.Count; i++)
            {
                if (textBox6.Text.Trim() == data.Rows[i]["手机号"].ToString().Trim())
                {
                    flag2 = 1;
                    record2 = i;
                }
            }

            if ((flag == 1 && flag1 == 1 && flag2 == 1) && (record == record1 && record1 == record2))
            {
                textBox1.Text = data.Rows[record]["管理员编号"].ToString().Trim();
                textBox2.Text = data.Rows[record]["登录名"].ToString().Trim();
                textBox3.Text = data.Rows[record]["密码"].ToString().Trim();
                BianHao = textBox1.Text;
                MiMa = textBox3.Text;

                textBox4.Text = "";
                textBox5.Text = "";
                textBox6.Text = "";

                MessageBox.Show("已成功找回");
            }
            else
            {
                MessageBox.Show("找回失败，请仔细查看信息是否填写完整及正确！");
            }
        }
        
        //返回上一界面
        private void button3_Click(object sender, EventArgs e)
        {
            this.Dispose();
            登录界面 f = new 登录界面();
            f.Show();
        }

        //清空
        private void button2_Click(object sender, EventArgs e)
        {

            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox6.Text = "";
        }

        //修改密码
        private void button4_Click(object sender, EventArgs e)
        {
            管理员修改密码 f = new 管理员修改密码();
            f.Show();
        }
    }
}
