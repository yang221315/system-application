﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 系统应用开发
{
    public partial class 学生修改密码 : Form
    {
        public 学生修改密码()
        {
            InitializeComponent();
            textBox1.Text = 学生找回密码.MiMa;

            String startupPath = Application.StartupPath;
            startupPath = startupPath.Substring(0, startupPath.LastIndexOf("\\"));
            string path = startupPath.Substring(0, startupPath.LastIndexOf("\\"));

            this.BackgroundImage = Image.FromFile(path + @"\\图片素材\\修改密码背景1.jpg");
            this.BackgroundImageLayout = ImageLayout.Stretch;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox2.Text.Trim() == textBox3.Text.Trim())
            {
                Sql sql = new Sql();
                String s = "update [学生表] set 密码 = @text where 用户编号 = @text1";
                SqlParameter[] sqlParameters =
                {
                    new SqlParameter("text",textBox2.Text.Trim()),
                    new SqlParameter("text1",学生找回密码.BianHao),
                };
                int result = sql.NonQuery(s,sqlParameters);

                if (result == 0)
                {
                    MessageBox.Show("修改失败！");
                }
                else
                {
                    MessageBox.Show("修改密码成功！");
                }
            }
            else
            {
                MessageBox.Show("两次密码输入不一致！");
                textBox2.Text = "";
                textBox3.Text = "";
            }
        }

        //退出
        private void button2_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}
