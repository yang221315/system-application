﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using iText.Layout.Properties;
using 系统应用开发.管理员模块;

namespace 系统应用开发
{
    public partial class 登录界面 : Form
    {
        static public String TEXT;
        public 登录界面()
        {
            InitializeComponent();
            string startupPath = Application.StartupPath;
            startupPath = startupPath.Substring(0, startupPath.LastIndexOf("\\"));
            string path = startupPath.Substring(0, startupPath.LastIndexOf("\\"));

            this.BackgroundImage = Image.FromFile(path+ @"\\图片素材\\星空1.jpg");
            this.BackgroundImageLayout = ImageLayout.Stretch;
            this.panel2.BackgroundImage = Image.FromFile(path+@"\\图片素材\\超跑.jpg");
            this.panel2.BackgroundImageLayout = ImageLayout.Stretch;
            this.panel1.BackgroundImage = Image.FromFile(path+@"\\图片素材\\天空.jpg");
            this.panel1.BackgroundImageLayout = ImageLayout.Stretch;
        }

        //登录
        private void button2_Click(object sender, EventArgs e)
        {
            TEXT = textBox1.Text;
            Sql sql = new Sql();

            if (comboBox1.Text.Trim() == "学生")
            {
                String s = "select 登录名,密码 from [学生表] where 登录名 = @username and 密码 = @userpassword";
                SqlParameter[] sqlParameters =
                {
                    new SqlParameter("username",textBox1.Text.Trim()),
                    new SqlParameter("userpassword",textBox2.Text.Trim()),
                };

                int result = sql.Login(s, sqlParameters);

                if (result == 0)
                {
                    MessageBox.Show("用户名或密码错误！");
                    textBox1.Text = "";
                    textBox2.Text = "";
                }
                else
                {
                    MessageBox.Show("登录成功！");
                    学生主界面 form = new 学生主界面();
                    form.Show();
                    this.Hide();
                }
            }

            if (comboBox1.Text.Trim() == "老师")
            {
                String s = "select 登录名,密码 from [教师表] where 登录名 = @username and 密码 = @userpassword";
                SqlParameter[] sqlParameters =
                {
                    new SqlParameter("username",textBox1.Text.Trim()),
                    new SqlParameter("userpassword",textBox2.Text.Trim()),
                };

                int result = sql.Login(s, sqlParameters);

                if (result == 0)
                {
                    MessageBox.Show("用户名或密码错误！");
                    textBox1.Text = "";
                    textBox2.Text = "";
                }
                else
                {
                    MessageBox.Show("登录成功！");
                    老师主界面 form = new 老师主界面();
                    form.Show();
                    this.Hide();
                }
            }

            if (comboBox1.Text.Trim() == "管理员")
            {
                String s = "select 登录名,密码 from [管理员表] where 登录名 = @username and 密码 = @userpassword";
                SqlParameter[] sqlParameters =
                {
                    new SqlParameter("username",textBox1.Text.Trim()),
                    new SqlParameter("userpassword",textBox2.Text.Trim()),
                };

                int result = sql.Login(s, sqlParameters);

                if (result == 0)
                {
                    MessageBox.Show("用户名或密码错误！");
                    textBox1.Text = "";
                    textBox2.Text = "";
                }
                else
                {
                    MessageBox.Show("登录成功！");
                    管理员主界面 form = new 管理员主界面();
                    form.Show();
                    this.Hide();
                }
            }
        }

        //注册
        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text.Trim() == "学生")
            {
                学生注册界面 form = new 学生注册界面();
                this.Hide();
                form.Show();
            }
            if (comboBox1.Text.Trim() == "老师")
            {
                教师注册界面 form = new 教师注册界面();
                this.Hide();
                form.Show();
            }
            if (comboBox1.Text.Trim() == "管理员")
            {
                管理员注册界面 form = new 管理员注册界面();
                this.Hide();
                form.Show();
            }
        }

        #region 退出提示
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("点击关闭后，程序会自动退出！", "！！！提示", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                e.Cancel = false;
                System.Environment.Exit(0);
                Application.Exit();
            }
            else
            {
                e.Cancel = true;
            }
        }
        #endregion

        //忘记密码
        private void label5_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text.Trim() == "学生")
            {
                学生找回密码 form = new 学生找回密码();
                this.Hide();
                form.Show();
            }
            if (comboBox1.Text.Trim() == "老师")
            {
                教师找回密码 form = new 教师找回密码();
                this.Hide();
                form.Show();
            }
            if (comboBox1.Text.Trim() == "管理员")
            {
                管理员找回密码 form = new 管理员找回密码();
                this.Hide();
                form.Show();
            }
        }
    }
}
