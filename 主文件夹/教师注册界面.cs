﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 系统应用开发
{
    public partial class 教师注册界面 : Form
    {
        public 教师注册界面()
        {
            InitializeComponent();

            string startupPath = Application.StartupPath;
            startupPath = startupPath.Substring(0, startupPath.LastIndexOf("\\"));
            string path = startupPath.Substring(0, startupPath.LastIndexOf("\\"));

            SetGifBackground(path + @"\\图片素材\\背景2.gif");
            //防止背景闪烁
            SetStyle(ControlStyles.UserPaint, true);
            SetStyle(ControlStyles.AllPaintingInWmPaint, true); 
            SetStyle(ControlStyles.DoubleBuffer, true);

            //位置屏幕居中
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;

            Sql sql = new Sql();
            String s = "select 用户编号,教师号 from [教师表]";

            DataSet dss = new DataSet();
            dss = sql.Query(s);
            DataTable data = dss.Tables["table1"];

            int zhi = Convert.ToInt32(data.Rows[data.Rows.Count - 1]["用户编号"].ToString()) + 1;
            string ID = data.Rows[data.Rows.Count - 1]["教师号"].ToString();
            textBox1.Text = zhi.ToString().Trim();
            textBox7.Text = ID.ToString().Trim();
        }

        //使用动态gif当背景
        private void SetGifBackground(string gifPath)
        {
            Image gif = Image.FromFile(gifPath);
            System.Drawing.Imaging.FrameDimension fd = new System.Drawing.Imaging.FrameDimension(gif.FrameDimensionsList[0]);
            int count = gif.GetFrameCount(fd);
            Timer giftimer = new Timer();
            giftimer.Interval = 100;
            int i = 0;
            Image bgImg = null;
            giftimer.Tick += (s, e) =>
            {
                if (i >= count)
                {
                    i = 0;
                }

                gif.SelectActiveFrame(fd, i);
                System.IO.Stream stream = new System.IO.MemoryStream();
                gif.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);

                if (bgImg != null)
                {
                    bgImg.Dispose();
                }

                bgImg = Image.FromStream(stream);
                this.BackgroundImage = bgImg;
                this.BackgroundImageLayout = ImageLayout.Stretch;
                i++;
            };
            giftimer.Start();
        }

        //返回上一界面
        private void button3_Click(object sender, EventArgs e)
        {
            this.Dispose();
            登录界面 f = new 登录界面();
            f.Show();
        }

        //取消注册
        private void button2_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox6.Text = "";
            textBox7.Text = "";
        }

        #region 退出提示
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("点击关闭后，程序会自动退出！", "！！！提示", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                e.Cancel = false;
                System.Environment.Exit(0);
            }
            else
            {
                e.Cancel = true;
            }
        }
        #endregion

        private void button1_Click(object sender, EventArgs e)
        {
            int flag = 0, flag1 = 0, flag2 = 0, flag3 = 0, flag4 = 0, flag5 = 0,flag6 = 0;
            Sql sql = new Sql();

            //查询教师表
            String s = "select * from [教师表]";

            DataSet ds = new DataSet();
            ds = sql.Query(s);
            DataTable datable = ds.Tables["table1"];

            //判断编号是否填写以及正确与否
            if (textBox1.Text.Trim() == "")
            {
                MessageBox.Show("用户编号未填写！", "注意！！！");
                flag = 0;
            }
            else
            {
                int bandu = 0;
                for (int i = 0; i < datable.Rows.Count; i++)
                {
                    if (datable.Rows[i]["用户编号"].ToString().Trim() == textBox1.Text.Trim())
                    {
                        bandu = 1;
                    }
                }

                if (bandu == 1)
                {
                    MessageBox.Show("已存在该编号，请新填写", "注意！！！");
                }
                else
                {
                    flag = 1;
                }
            }

            //判断登录名是否填写以及有无重复
            if (textBox2.Text.Trim() == "")
            {
                MessageBox.Show("登录名未填写！", "注意！！！");
                flag1 = 0;
            }
            else
            {
                int bandu = 0;
                for (int i = 0; i < datable.Rows.Count; i++)
                {
                    if (datable.Rows[i]["登录名"].ToString().Trim() == textBox2.Text.Trim())
                    {
                        bandu = 1;
                    }
                }

                if (bandu == 1)
                {
                    MessageBox.Show("已存在该登录名，请新填写", "注意！！！");
                }

                else
                {
                    flag1 = 1;
                }
            }

            //判断密码是否填写
            if (textBox3.Text.Trim() == "")
            {
                MessageBox.Show("密码未填写！", "注意！！！");
                flag2 = 0;
            }
            else
            {
                flag2 = 1;
            }

            //判断姓名是否填写
            if (textBox4.Text.Trim() == "")
            {
                MessageBox.Show("姓名未填写！", "注意！！！");
                flag3 = 0;
            }
            else
            {
                flag3 = 1;
            }

            //判断身份证是否填写以及有无重复
            if (textBox5.Text.Trim() == "")
            {
                MessageBox.Show("身份证未填写！", "注意！！！");
                flag4 = 0;
            }
            else
            {
                int bandu = 0;
                for (int i = 0; i < datable.Rows.Count; i++)
                {
                    if (datable.Rows[i]["身份证"].ToString().Trim() == textBox5.Text.Trim())
                    {
                        bandu = 1;
                    }
                }

                if (bandu == 1)
                {
                    MessageBox.Show("已存在该身份证，请新填写", "注意！！！");
                }

                else
                {
                    flag4 = 1;
                }
            }

            //判断手机号是否填写以及有无重复
            if (textBox6.Text.Trim() == "")
            {
                MessageBox.Show("手机号未填写！", "注意！！！");
                flag5 = 0;
            }
            else
            {
                int bandu = 0;
                for (int i = 0; i < datable.Rows.Count; i++)
                {
                    if (datable.Rows[i]["手机号"].ToString().Trim() == textBox6.Text.Trim())
                    {
                        bandu = 1;
                    }
                }

                if (bandu == 1)
                {
                    MessageBox.Show("已存在该手机号，请新填写", "注意！！！");
                }

                else
                {
                    flag5 = 1;
                }
            }

            //判断教师号是否填写以及有无重复
            if (textBox7.Text.Trim() == "")
            {
                MessageBox.Show("教师号未填写！", "注意！！！");
                flag6 = 0;
            }
            else
            {
                int bandu = 0;
                for (int i = 0; i < datable.Rows.Count; i++)
                {
                    if (datable.Rows[i]["教师号"].ToString().Trim() == textBox7.Text.Trim())
                    {
                        bandu = 1;
                    }
                }

                if (bandu == 1)
                {
                    MessageBox.Show("已存在该教师号，请新填写", "注意！！！");
                }
                else
                {
                    flag6 = 1;
                }
            }

            if (flag == 1 && flag1 == 1 && flag2 == 1 && flag3 == 1 && flag4 == 1 && flag5 == 1 && flag6 == 1)
            {
                s = "insert into [教师表] values (@userID,@loginname,@userpwd,@username,@Card,@phonenumber,@TeacherID)";

                SqlParameter[] sqlParameters =
                {
                    new SqlParameter("userID",textBox1.Text.Trim()),
                    new SqlParameter("loginname",textBox2.Text.Trim()),
                    new SqlParameter("userpwd",textBox3.Text.Trim()),
                    new SqlParameter("username",textBox4.Text.Trim()),
                    new SqlParameter("Card",textBox5.Text.Trim()),
                    new SqlParameter("phonenumber",textBox6.Text.Trim()),
                    new SqlParameter("TeacherID",textBox7.Text.Trim()),
                };

                int result = sql.NonQuery(s, sqlParameters);

                if (result == 0)
                {
                    MessageBox.Show("注册失败，请重新尝试！");
                }
                else
                {
                    MessageBox.Show("注册成功！");
                    textBox1.Text = "";
                    textBox2.Text = "";
                    textBox3.Text = "";
                    textBox4.Text = "";
                    textBox5.Text = "";
                    textBox6.Text = "";
                    textBox7.Text = "";
                }
            }
        }
    }
}
