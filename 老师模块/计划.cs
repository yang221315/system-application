﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 系统应用开发
{
    public partial class 计划 : Form
    {
        public 计划()
        {
            InitializeComponent();
            string startupPath = Application.StartupPath;
            startupPath = startupPath.Substring(0, startupPath.LastIndexOf("\\"));
            string path = startupPath.Substring(0, startupPath.LastIndexOf("\\"));
            this.BackgroundImage = Image.FromFile(path+@"\\图片素材\\老师背景7.jpg");
            this.BackgroundImageLayout = ImageLayout.Stretch;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            label1.Text = DateTime.Now.ToString("yyyy") + "年的计划";
        }

        //写入txt文件
        private int WriteListToTxt()
        {
            string startupPath = Application.StartupPath;
            startupPath = startupPath.Substring(0, startupPath.LastIndexOf("\\"));
            string path1 = startupPath.Substring(0, startupPath.LastIndexOf("\\"));
            String path = path1+ @"\\计划安排\\" + 登录界面.TEXT + "的计划安排.txt";
            try
            {
                int count = listBox1.Items.Count;
                StreamWriter _wstream = new StreamWriter(path);
                for (int i = 0; i < count; i++)
                {
                    string data = listBox1.Items[i].ToString();
                    _wstream.Write(data);
                    _wstream.WriteLine();
                }
                _wstream.Flush();
                _wstream.Close();

                return 0;
            }
            catch(Exception e)
            {
                MessageBox.Show("找不到路径");
                return 1;
            }
        }

        //从txt文件中读取数据
        private int ReadTxtToList()
        {
            string startupPath = Application.StartupPath;
            startupPath = startupPath.Substring(0, startupPath.LastIndexOf("\\"));
            string path1 = startupPath.Substring(0, startupPath.LastIndexOf("\\"));
            String path = path1+@"\\计划安排\\" + 登录界面.TEXT + "的计划安排.txt";
            try
            {
                StreamReader _rstream = new StreamReader(path, System.Text.Encoding.UTF8);
                string line;
                while ((line = _rstream.ReadLine()) != null)
                {
                    listBox1.Items.Add(line);
                }
                _rstream.Close();
                return 0;
            }
            catch(Exception e)
            {
                MessageBox.Show("找不到路径");
                return 1;
            }
        }

        //写数据
        private void button1_Click(object sender, EventArgs e)
        {
            int result = WriteListToTxt();
            if(result==0)
                MessageBox.Show("数据导入成功");
        }

        //读数据
        private void button2_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            int result = ReadTxtToList();
            if(result==0)
                MessageBox.Show("数据导出成功");
        }

        //删除计划
        private void button3_Click(object sender, EventArgs e)
        {
            listBox1.Items.Remove(textBox1.Text);
            textBox1.Text = "";
        }

        //增加计划
        private void button4_Click(object sender, EventArgs e)
        {
            listBox1.Items.Add(textBox1.Text);
            textBox1.Text = "";
        }

        //修改计划
        private void button5_Click(object sender, EventArgs e)
        {
            listBox1.Items.Remove(textBox2.Text);
            listBox1.Items.Add(textBox3.Text);
            textBox2.Text = "";
            textBox3.Text = "";
        }
    }
}
