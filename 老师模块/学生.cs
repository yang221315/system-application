﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 系统应用开发
{
    public partial class 学生 : Form
    {
        Sql sql = new Sql();
        String name;
        public 学生()
        {
            InitializeComponent();
            string startupPath = Application.StartupPath;
            startupPath = startupPath.Substring(0, startupPath.LastIndexOf("\\"));
            string path = startupPath.Substring(0, startupPath.LastIndexOf("\\"));
            this.BackgroundImage = Image.FromFile(path+@"\\图片素材\\老师背景3.jpg");
            this.BackgroundImageLayout = ImageLayout.Stretch;

            String s = "select * from [教师表]";
            DataSet ds = new DataSet();
            ds = sql.Query(s);
            DataTable datable = ds.Tables["table1"];
            dataGridView1.DataSource = datable;

            for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
            {
                for (int j = 0; j < dataGridView1.Columns.Count; j++)
                {
                    if (dataGridView1.Rows[i].Cells[j].Value.ToString().Trim() == 登录界面.TEXT.ToString())
                    {
                        name = dataGridView1.Rows[i].Cells[j + 2].Value.ToString().Trim();
                        break;
                    }
                }
            }

            s = "select * from [" + name + "的学生表]";
            ds = sql.Query(s);
            datable = ds.Tables["table1"];
            dataGridView1.DataSource = datable;

            label1.Text = name + "老师的学生表";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            String s = "update [" + name + "的学生表] set 身份证 = @text2,手机号 = @text3 where 姓名 = @text1";

            SqlParameter[] sqlParameters =
            {
                new SqlParameter("text1",textBox1.Text.Trim()),
                new SqlParameter("text2",textBox2.Text.Trim()),
                new SqlParameter("text3",textBox3.Text.Trim()),
            };

            int flag = sql.NonQuery(s, sqlParameters);

            if (flag == 0)
            {
                MessageBox.Show("修改失败，请重新尝试！", "注意！！！");
            }
            else
            {
                MessageBox.Show("修改成功！");
                button1_attach();
                textBox1.Text = "";
                textBox2.Text = "";
                textBox3.Text = "";
            }
        }

        public void button1_attach()
        {
            String s = "select * from [" + name + "的学生表]";
            DataTable table = new DataTable();
            DataSet ds = new DataSet();
            ds = sql.Query(s);
            DataTable datable = ds.Tables["table1"];
            dataGridView1.DataSource = datable;
        }

        private void dataGridView1_Click(object sender, DataGridViewCellEventArgs e)
        {
            int line = e.RowIndex;
            if (line >= 0)
            {
                textBox1.Text = dataGridView1.Rows[line].Cells[0].Value.ToString();
                textBox2.Text = dataGridView1.Rows[line].Cells[1].Value.ToString();
                textBox3.Text = dataGridView1.Rows[line].Cells[2].Value.ToString();
            }
        }
    }
}
