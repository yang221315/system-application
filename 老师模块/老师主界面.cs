﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 系统应用开发
{
    public partial class 老师主界面 : Form
    {
        Control[] controls = new Control[100];
        int count = 0;
        public 老师主界面()
        {
            InitializeComponent();
            string startupPath = Application.StartupPath;
            startupPath = startupPath.Substring(0, startupPath.LastIndexOf("\\"));
            string path = startupPath.Substring(0, startupPath.LastIndexOf("\\"));

            this.BackgroundImage = Image.FromFile(path+@"\\图片素材\\老师背景1.jpg");
            this.BackgroundImageLayout = ImageLayout.Stretch;

            this.panel4.BackgroundImage = Image.FromFile(@"E:\\visual studio practice\\系统应用开发\\图片素材\\老师背景2.jpg");
            this.panel4.BackgroundImageLayout = ImageLayout.Stretch;

            label5.Text ="Role:"+ 登录界面.TEXT.Trim();

            foreach (Control c in panel4.Controls)
            {
                controls[count++] = c;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            label7.Text = DateTime.Now.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            panel4.Controls.Clear();
            panel4.Controls.AddRange(controls);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            课程安排 f = new 课程安排();
            f.TopLevel = false;
            panel4.Controls.Clear();
            panel4.Controls.Add(f);
            f.Show();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("点击关闭后，返回上一个界面！", "！！！提示", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                this.Dispose();
                登录界面 form = new 登录界面();
                form.Show();
            }
        }

        private void pictureBox8_Click(object sender, EventArgs e)
        {

            课程安排 f = new 课程安排();
            f.TopLevel = false;
            panel4.Controls.Clear();
            panel4.Controls.Add(f);
            f.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            计划 f = new 计划();
            f.TopLevel = false;
            panel4.Controls.Clear();
            panel4.Controls.Add(f);
            f.Show();
        }

        private void pictureBox11_Click(object sender, EventArgs e)
        {
            计划 f = new 计划();
            f.TopLevel = false;
            panel4.Controls.Clear();
            panel4.Controls.Add(f);
            f.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            布置作业 f = new 布置作业();
            f.TopLevel = false;
            panel4.Controls.Clear();
            panel4.Controls.Add(f);
            f.Show();
        }

        private void pictureBox12_Click(object sender, EventArgs e)
        {
            布置作业 f = new 布置作业();
            f.TopLevel = false;
            panel4.Controls.Clear();
            panel4.Controls.Add(f);
            f.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            学生 f = new 学生();
            f.TopLevel = false;
            panel4.Controls.Clear();
            panel4.Controls.Add(f);
            f.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            老师账号 f = new 老师账号();
            f.TopLevel = false;
            panel4.Controls.Clear();
            panel4.Controls.Add(f);
            f.Show();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            教师设置 f = new 教师设置();
            f.TopLevel = false;
            panel4.Controls.Clear();
            panel4.Controls.Add(f);
            f.Show();
        }
        #region 退出提示
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("点击关闭后，程序会自动退出！", "！！！提示", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                e.Cancel = false;
                System.Environment.Exit(0);
                Application.Exit();
            }
            else
            {
                e.Cancel = true;
            }
        }
        #endregion
    }
}
