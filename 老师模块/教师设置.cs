﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 系统应用开发
{
    public partial class 教师设置 : Form
    {
        Sql sql = new Sql();

        public 教师设置()
        {
            InitializeComponent();
            string startupPath = Application.StartupPath;
            startupPath = startupPath.Substring(0, startupPath.LastIndexOf("\\"));
            string path = startupPath.Substring(0, startupPath.LastIndexOf("\\"));
            this.BackgroundImage = Image.FromFile(path+@"\\图片素材\\老师背景6.jpg");
            this.BackgroundImageLayout = ImageLayout.Stretch;
        }

        //退出账号
        private void button1_Click(object sender, EventArgs e)
        {
            this.Parent.Parent.Dispose();
            登录界面 f = new 登录界面();
            f.Show();
        }

        //注销账号
        private void button2_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("确定注销当前账户！", "！！！提示", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                String s = "delete from [教师表] where 登录名 = @text";
                SqlParameter[] sqlParameters =
                {
                    new SqlParameter("text",登录界面.TEXT.ToString().Trim()),
                };

                int flag = sql.NonQuery(s, sqlParameters);

                if (flag == 0)
                {
                    MessageBox.Show("删除失败，请重新尝试！");
                }
                else
                {
                    MessageBox.Show("账号注销成功！");
                }
            }
        }

        //设置背景颜色
        private void button3_Click(object sender, EventArgs e)
        {
            colorDialog1.ShowDialog();
            this.BackColor = this.colorDialog1.Color;
            this.Parent.Parent.BackColor = this.colorDialog1.Color;
        }

        //设置字体颜色
        private void button4_Click(object sender, EventArgs e)
        {
            colorDialog2.ShowDialog();

            this.button1.ForeColor = this.colorDialog2.Color;

            this.button2.ForeColor = this.colorDialog2.Color;

            this.button3.ForeColor = this.colorDialog2.Color;

            this.button4.ForeColor = this.colorDialog2.Color;

            this.button5.ForeColor = this.colorDialog2.Color;

            this.Parent.Parent.ForeColor = this.colorDialog2.Color;
        }

        //设置字体大小
        private void button5_Click(object sender, EventArgs e)
        {
            fontDialog1.ShowDialog();

            this.button1.Font = this.fontDialog1.Font;

            this.button2.Font = this.fontDialog1.Font;

            this.button3.Font = this.fontDialog1.Font;

            this.button4.Font = this.fontDialog1.Font;

            this.button5.Font = this.fontDialog1.Font;

            this.Parent.Parent.Font = this.fontDialog1.Font;
        }
    }
}
