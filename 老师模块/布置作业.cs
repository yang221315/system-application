﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 系统应用开发
{
    public partial class 布置作业 : Form
    {
        Sql sql = new Sql();
        public 布置作业()
        {
            InitializeComponent();
            string startupPath = Application.StartupPath;
            startupPath = startupPath.Substring(0, startupPath.LastIndexOf("\\"));
            string path = startupPath.Substring(0, startupPath.LastIndexOf("\\"));
            this.BackgroundImage = Image.FromFile(path+@"\\图片素材\\老师背景8.jpg");
            this.BackgroundImageLayout = ImageLayout.Stretch;

            String s = "select * from [作业表]";
            DataSet ds = new DataSet();
            ds = sql.Query(s);
            DataTable datable = ds.Tables["table1"];
            dataGridView1.DataSource = datable;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            label2.Text = "今天是：" + DateTime.Now.ToString() + "  " + DateTime.Now.DayOfWeek;
        }


        //点击功能
        private void dataGridView1_Click(object sender, DataGridViewCellEventArgs e)
        {
            int line = e.RowIndex;
            if (line >= 0)
            {
                textBox1.Text = dataGridView1.Rows[line].Cells[0].Value.ToString();
                textBox2.Text = dataGridView1.Rows[line].Cells[1].Value.ToString();
                textBox3.Text = dataGridView1.Rows[line].Cells[2].Value.ToString();
                textBox4.Text = dataGridView1.Rows[line].Cells[3].Value.ToString();
                textBox5.Text = dataGridView1.Rows[line].Cells[4].Value.ToString();
                textBox6.Text = dataGridView1.Rows[line].Cells[5].Value.ToString();
                textBox7.Text = dataGridView1.Rows[line].Cells[6].Value.ToString();
                textBox8.Text = dataGridView1.Rows[line].Cells[7].Value.ToString();
                textBox9.Text = dataGridView1.Rows[line].Cells[8].Value.ToString();
            }
        }

        //修改功能实现
        private void button1_Click(object sender, EventArgs e)
        {
            String s = "update [作业表] set 语文作业 = @text1,数学作业 = @text2,英语作业 = @text3," +
                "物理作业 = @text4,化学作业 = @text5,生物作业 = @text6,地理作业 = @text7," +
                "政治作业 = @text8,历史作业 = @text9";

            SqlParameter[] sqlParameters =
            {
                new SqlParameter("text1",textBox1.Text.Trim()),
                new SqlParameter("text2",textBox2.Text.Trim()),
                new SqlParameter("text3",textBox3.Text.Trim()),
                new SqlParameter("text4",textBox4.Text.Trim()),
                new SqlParameter("text5",textBox5.Text.Trim()),
                new SqlParameter("text6",textBox6.Text.Trim()),
                new SqlParameter("text7",textBox7.Text.Trim()),
                new SqlParameter("text8",textBox8.Text.Trim()),
                new SqlParameter("text9",textBox9.Text.Trim()),
            };

            int flag = sql.NonQuery(s, sqlParameters);

            if (flag == 0)
            {
                MessageBox.Show("修改失败，请重新尝试！", "注意！！！");
            }
            else
            {
                MessageBox.Show("修改成功！");
                button1_attach();
            }
        }
        public void button1_attach()
        {
            String s = "select * from [作业表]";
            DataTable table = new DataTable();
            DataSet ds = new DataSet();
            ds = sql.Query(s);
            DataTable datable = ds.Tables["table1"];
            dataGridView1.DataSource = datable;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox6.Text = "";
            textBox7.Text = "";
            textBox8.Text = "";
            textBox9.Text = "";
        }
    }
}
