﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 系统应用开发
{
    public partial class 管理员账户 : Form
    {
        Sql sql = new Sql();
        public 管理员账户()
        {
            InitializeComponent();
            string startupPath = Application.StartupPath;
            startupPath = startupPath.Substring(0, startupPath.LastIndexOf("\\"));
            string path = startupPath.Substring(0, startupPath.LastIndexOf("\\"));
            this.BackgroundImage = Image.FromFile(path+@"\\图片素材\\管理员背景6.jpg");
            this.BackgroundImageLayout = ImageLayout.Stretch;

            String s = "select * from [管理员表] where [登录名] = '" + 登录界面.TEXT.ToString().Trim() + "'";
            DataSet ds = new DataSet();
            ds = sql.Query(s);
            DataTable datable = ds.Tables["table1"];
            dataGridView1.DataSource = datable;
        }

        private void dataGridView1_Click(object sender, DataGridViewCellEventArgs e)
        {
            int line = e.RowIndex;
            if (line >= 0)
            {
                textBox1.Text = dataGridView1.Rows[line].Cells[0].Value.ToString();
                textBox2.Text = dataGridView1.Rows[line].Cells[1].Value.ToString();
                textBox3.Text = dataGridView1.Rows[line].Cells[2].Value.ToString();
                textBox4.Text = dataGridView1.Rows[line].Cells[3].Value.ToString();
                textBox5.Text = dataGridView1.Rows[line].Cells[4].Value.ToString();
                textBox6.Text = dataGridView1.Rows[line].Cells[5].Value.ToString();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            String s = "update [管理员表] set 登录名 = @text2,密码 = @text3," +
                "姓名 = @text4,身份证 = @text5,手机号 = @text6 where 管理员编号 = @text1";
            SqlParameter[] sqlParameters =
            {
                new SqlParameter("text1",textBox1.Text.Trim()),
                new SqlParameter("text2",textBox2.Text.Trim()),
                new SqlParameter("text3",textBox3.Text.Trim()),
                new SqlParameter("text4",textBox4.Text.Trim()),
                new SqlParameter("text5",textBox5.Text.Trim()),
                new SqlParameter("text6",textBox6.Text.Trim()),
            };

            int flag = sql.NonQuery(s, sqlParameters);

            if (textBox1.Text == "" || textBox2.Text == "" || textBox3.Text == ""
               || textBox4.Text == "" || textBox5.Text == "" || textBox6.Text == "")
            {
                flag = 0;
            }

            if (flag == 0)
            {
                MessageBox.Show("修改失败，请重新尝试！", "注意！！！");
            }
            else
            {
                MessageBox.Show("修改成功！");
                button1_attach();
                textBox1.Text = "";
                textBox2.Text = "";
                textBox3.Text = "";
                textBox4.Text = "";
                textBox5.Text = "";
                textBox6.Text = "";
            }
        }

        public void button1_attach()
        {
            String s = "select * from [管理员表] where [登录名] = '" + 登录界面.TEXT.ToString().Trim() + "'";
            DataTable table = new DataTable();
            DataSet ds = new DataSet();
            ds = sql.Query(s);
            DataTable datable = ds.Tables["table1"];
            dataGridView1.DataSource = datable;
        }
    }
}
