﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 系统应用开发
{
    public partial class 教师用户 : Form
    {
        Sql sql = new Sql();
        public 教师用户()
        {
            InitializeComponent();
            string startupPath = Application.StartupPath;
            startupPath = startupPath.Substring(0, startupPath.LastIndexOf("\\"));
            string path = startupPath.Substring(0, startupPath.LastIndexOf("\\"));
            this.BackgroundImage = Image.FromFile(path+@"\\图片素材\\管理员背景5.jpg");
            this.BackgroundImageLayout = ImageLayout.Stretch;

            String s = "select * from [教师表]";
            DataSet ds = new DataSet();
            ds = sql.Query(s);
            DataTable datable = ds.Tables["table1"];
            dataGridView1.DataSource = datable;
        }

        #region 增加操作
        private void button2_Click(object sender, EventArgs e)
        {
            String s = "insert into [教师表] values (@userID,@loginname,@userpwd,@username,@Card,@phonenumber,@IDnumber)";

            SqlParameter[] sqlParameters =
            {
                new SqlParameter("userID",textBox1.Text.Trim()),
                new SqlParameter("loginname",textBox2.Text.Trim()),
                new SqlParameter("userpwd",textBox3.Text.Trim()),
                new SqlParameter("username",textBox4.Text.Trim()),
                new SqlParameter("Card",textBox5.Text.Trim()),
                new SqlParameter("phonenumber",textBox6.Text.Trim()),
                new SqlParameter("IDnumber",textBox7.Text.Trim()),
            };

            int flag = 0;

            if (textBox1.Text == "" || textBox2.Text == "" || textBox3.Text == ""
                || textBox4.Text == "" || textBox5.Text == "" || textBox6.Text == "" || textBox6.Text == "")
            {
                flag = 0;
            }
            else
            {
                flag = sql.NonQuery(s, sqlParameters);
            }

            if (flag == 0)
            {
                MessageBox.Show("增加失败，请重新尝试！");
            }
            else
            {
                MessageBox.Show("增加成功！");
                button3_attach();
                textBox1.Text = "";
                textBox2.Text = "";
                textBox3.Text = "";
                textBox4.Text = "";
                textBox5.Text = "";
                textBox6.Text = "";
                textBox7.Text = "";
            }
        }
        #endregion

        #region 点击功能
        private void dataGridView1_Click(object sender, DataGridViewCellEventArgs e)
        {
            int line = e.RowIndex;
            if (line >= 0)
            {
                textBox1.Text = dataGridView1.Rows[line].Cells[0].Value.ToString();
                textBox2.Text = dataGridView1.Rows[line].Cells[1].Value.ToString();
                textBox3.Text = dataGridView1.Rows[line].Cells[2].Value.ToString();
                textBox4.Text = dataGridView1.Rows[line].Cells[3].Value.ToString();
                textBox5.Text = dataGridView1.Rows[line].Cells[4].Value.ToString();
                textBox6.Text = dataGridView1.Rows[line].Cells[5].Value.ToString();
                textBox7.Text = dataGridView1.Rows[line].Cells[6].Value.ToString();
            }
        }
        #endregion

        #region 删除操作
        private void button4_Click(object sender, EventArgs e)
        {
            String s = "delete from [教师表] where 用户编号 = @userID or 登录名 = @loginname or 密码 = @userpwd" +
                " or 姓名 = @username or 身份证 = @Card or 手机号 = @phonenumber or 教师号 = @IDnumber";
            SqlParameter[] sqlParameters =
            {
                new SqlParameter("userID",textBox1.Text.Trim()),
                new SqlParameter("loginname",textBox2.Text.Trim()),
                new SqlParameter("userpwd",textBox3.Text.Trim()),
                new SqlParameter("username",textBox4.Text.Trim()),
                new SqlParameter("Card",textBox5.Text.Trim()),
                new SqlParameter("phonenumber",textBox6.Text.Trim()),
                new SqlParameter("IDnumber",textBox7.Text.Trim()),
            };

            int flag = sql.NonQuery(s, sqlParameters);

            if (flag == 0)
            {
                MessageBox.Show("删除失败，请重新尝试！");
            }
            else
            {
                MessageBox.Show("删除成功！");
                button3_attach();
                textBox1.Text = "";
                textBox2.Text = "";
                textBox3.Text = "";
                textBox4.Text = "";
                textBox5.Text = "";
                textBox6.Text = "";
                textBox7.Text = "";
            }
        }
        #endregion

        #region 清空操作
        private void button5_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox6.Text = "";
            textBox7.Text = "";
        }
        #endregion

        #region 查询操作
        private void button1_Click(object sender, EventArgs e)
        {
            int flag = 0;

            //编号查询
            for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
            {
                for (int j = 0; j < dataGridView1.Columns.Count; j++)
                {
                    if (dataGridView1.Rows[i].Cells[j].Value.ToString() == textBox1.Text.ToString())
                    {
                        textBox2.Text = dataGridView1.Rows[i].Cells[j + 1].Value.ToString();
                        textBox3.Text = dataGridView1.Rows[i].Cells[j + 2].Value.ToString();
                        textBox4.Text = dataGridView1.Rows[i].Cells[j + 3].Value.ToString();
                        textBox5.Text = dataGridView1.Rows[i].Cells[j + 4].Value.ToString();
                        textBox6.Text = dataGridView1.Rows[i].Cells[j + 5].Value.ToString();
                        textBox7.Text = dataGridView1.Rows[i].Cells[j + 6].Value.ToString();
                        flag = 1;
                        break;
                    }
                }
            }

            //登录名查询
            for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
            {
                for (int j = 0; j < dataGridView1.Columns.Count; j++)
                {
                    if (dataGridView1.Rows[i].Cells[j].Value.ToString().Trim() == textBox2.Text.ToString())
                    {
                        textBox1.Text = dataGridView1.Rows[i].Cells[j - 1].Value.ToString();
                        textBox3.Text = dataGridView1.Rows[i].Cells[j + 1].Value.ToString();
                        textBox4.Text = dataGridView1.Rows[i].Cells[j + 2].Value.ToString();
                        textBox5.Text = dataGridView1.Rows[i].Cells[j + 3].Value.ToString();
                        textBox6.Text = dataGridView1.Rows[i].Cells[j + 4].Value.ToString();
                        textBox7.Text = dataGridView1.Rows[i].Cells[j + 5].Value.ToString();
                        flag = 1;
                        break;
                    }
                }
            }

            //密码查询
            for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
            {
                for (int j = 0; j < dataGridView1.Columns.Count; j++)
                {
                    if (dataGridView1.Rows[i].Cells[j].Value.ToString().Trim() == textBox3.Text.ToString())
                    {
                        textBox1.Text = dataGridView1.Rows[i].Cells[j - 2].Value.ToString();
                        textBox2.Text = dataGridView1.Rows[i].Cells[j - 1].Value.ToString();
                        textBox4.Text = dataGridView1.Rows[i].Cells[j + 1].Value.ToString();
                        textBox5.Text = dataGridView1.Rows[i].Cells[j + 2].Value.ToString();
                        textBox6.Text = dataGridView1.Rows[i].Cells[j + 3].Value.ToString();
                        textBox7.Text = dataGridView1.Rows[i].Cells[j + 4].Value.ToString();
                        flag = 1;
                        break;
                    }
                }
            }

            //姓名查询
            for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
            {
                for (int j = 0; j < dataGridView1.Columns.Count; j++)
                {
                    if (dataGridView1.Rows[i].Cells[j].Value.ToString().Trim() == textBox4.Text.ToString())
                    {
                        textBox1.Text = dataGridView1.Rows[i].Cells[j - 3].Value.ToString();
                        textBox2.Text = dataGridView1.Rows[i].Cells[j - 2].Value.ToString();
                        textBox3.Text = dataGridView1.Rows[i].Cells[j - 1].Value.ToString();
                        textBox5.Text = dataGridView1.Rows[i].Cells[j + 1].Value.ToString();
                        textBox6.Text = dataGridView1.Rows[i].Cells[j + 2].Value.ToString();
                        textBox7.Text = dataGridView1.Rows[i].Cells[j + 3].Value.ToString();
                        flag = 1;
                        break;
                    }
                }
            }

            //身份证查询
            for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
            {
                for (int j = 0; j < dataGridView1.Columns.Count; j++)
                {
                    if (dataGridView1.Rows[i].Cells[j].Value.ToString().Trim() == textBox5.Text.ToString())
                    {
                        textBox1.Text = dataGridView1.Rows[i].Cells[j - 4].Value.ToString();
                        textBox2.Text = dataGridView1.Rows[i].Cells[j - 3].Value.ToString();
                        textBox3.Text = dataGridView1.Rows[i].Cells[j - 2].Value.ToString();
                        textBox4.Text = dataGridView1.Rows[i].Cells[j - 1].Value.ToString();
                        textBox6.Text = dataGridView1.Rows[i].Cells[j + 1].Value.ToString();
                        textBox7.Text = dataGridView1.Rows[i].Cells[j + 2].Value.ToString();
                        flag = 1;
                        break;
                    }
                }
            }

            //手机号查询
            for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
            {
                for (int j = 0; j < dataGridView1.Columns.Count; j++)
                {
                    if (dataGridView1.Rows[i].Cells[j].Value.ToString().Trim() == textBox6.Text.ToString())
                    {
                        textBox1.Text = dataGridView1.Rows[i].Cells[j - 5].Value.ToString();
                        textBox2.Text = dataGridView1.Rows[i].Cells[j - 4].Value.ToString();
                        textBox3.Text = dataGridView1.Rows[i].Cells[j - 3].Value.ToString();
                        textBox4.Text = dataGridView1.Rows[i].Cells[j - 2].Value.ToString();
                        textBox5.Text = dataGridView1.Rows[i].Cells[j - 1].Value.ToString();
                        textBox7.Text = dataGridView1.Rows[i].Cells[j + 1].Value.ToString();
                        flag = 1;
                        break;
                    }
                }
            }

            //教师号查询
            for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
            {
                for (int j = 0; j < dataGridView1.Columns.Count; j++)
                {
                    if (dataGridView1.Rows[i].Cells[j].Value.ToString().Trim() == textBox7.Text.ToString())
                    {
                        textBox1.Text = dataGridView1.Rows[i].Cells[j - 6].Value.ToString();
                        textBox2.Text = dataGridView1.Rows[i].Cells[j - 5].Value.ToString();
                        textBox3.Text = dataGridView1.Rows[i].Cells[j - 4].Value.ToString();
                        textBox4.Text = dataGridView1.Rows[i].Cells[j - 3].Value.ToString();
                        textBox5.Text = dataGridView1.Rows[i].Cells[j - 2].Value.ToString();
                        textBox6.Text = dataGridView1.Rows[i].Cells[j - 1].Value.ToString();
                        flag = 1;
                        break;
                    }
                }
            }


            if (flag == 0)
            {
                MessageBox.Show("查询失败，请重试！");
            }
            else
            {
                MessageBox.Show("查询成功！");
            }
        }
        #endregion

        #region 修改操作
        private void button3_Click(object sender, EventArgs e)
        {
            String s = "update [教师表] set 登录名 = @loginname,密码 = @userpwd," +
                "姓名 = @username,身份证 = @Card,手机号 = @phonenumber," +
                "教师号 = @IDnumber where 用户编号 = @userID";
            SqlParameter[] sqlParameters =
            {
                new SqlParameter("userID",textBox1.Text.Trim()),
                new SqlParameter("loginname",textBox2.Text.Trim()),
                new SqlParameter("userpwd",textBox3.Text.Trim()),
                new SqlParameter("username",textBox4.Text.Trim()),
                new SqlParameter("Card",textBox5.Text.Trim()),
                new SqlParameter("phonenumber",textBox6.Text.Trim()),
                new SqlParameter("IDnumber",textBox7.Text.Trim()),
            };

            int flag = sql.NonQuery(s, sqlParameters);

            if (flag == 0)
            {
                MessageBox.Show("修改失败，请重新尝试！", "注意！！！");
            }
            else
            {
                MessageBox.Show("修改成功！");
                button3_attach();
                textBox1.Text = "";
                textBox2.Text = "";
                textBox3.Text = "";
                textBox4.Text = "";
                textBox5.Text = "";
                textBox6.Text = "";
                textBox7.Text = "";
            }
        }

        public void button3_attach()
        {
            String s = "select * from [教师表]";
            DataTable table = new DataTable();
            DataSet ds = new DataSet();
            ds = sql.Query(s);
            DataTable datable = ds.Tables["table1"];
            dataGridView1.DataSource = datable;
        }
        #endregion
    }
}
