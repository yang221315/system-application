﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 系统应用开发
{
    public partial class 学生主界面 : Form
    {
        Control[] controls = new Control[100];
        int count = 0;
        public 学生主界面()
        {
            InitializeComponent();
            string startupPath = Application.StartupPath;
            startupPath = startupPath.Substring(0, startupPath.LastIndexOf("\\"));
            string path = startupPath.Substring(0, startupPath.LastIndexOf("\\"));

            this.BackgroundImage = Image.FromFile(path+@"\\图片素材\\学生界面背景1.jpg");
            this.BackgroundImageLayout = ImageLayout.Stretch;

            this.panel4.BackgroundImage = Image.FromFile(path+@"\\图片素材\\学生界面背景1.jpg");
            this.panel4.BackgroundImageLayout = ImageLayout.Stretch;

            label5.Text = 登录界面.TEXT;

            foreach(Control c in panel4.Controls)
            {
                controls[count++] = c;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            label7.Text = DateTime.Now.ToString();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("点击关闭后，返回上一个界面！", "！！！提示", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                this.Dispose();
                登录界面 form = new 登录界面();
                form.Show();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            课程 f = new 课程();
            f.TopLevel = false;
            panel4.Controls.Clear();
            panel4.Controls.Add(f);
            f.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            目标 f = new 目标();
            f.TopLevel = false;
            panel4.Controls.Clear();
            panel4.Controls.Add(f);
            f.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            panel4.Controls.Clear();
            panel4.Controls.AddRange(controls);
        }

        private void pictureBox12_Click(object sender, EventArgs e)
        {
            课程 f = new 课程();
            f.TopLevel = false;
            panel4.Controls.Clear();
            panel4.Controls.Add(f);
            f.Show();
        }

        private void pictureBox13_Click(object sender, EventArgs e)
        {
            目标 f = new 目标();
            f.TopLevel = false;
            panel4.Controls.Clear();
            panel4.Controls.Add(f);
            f.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            学习计划 f = new 学习计划();
            f.TopLevel = false;
            panel4.Controls.Clear();
            panel4.Controls.Add(f);
            f.Show();
        }

        private void pictureBox14_Click(object sender, EventArgs e)
        {
            学习计划 f = new 学习计划();
            f.TopLevel = false;
            panel4.Controls.Clear();
            panel4.Controls.Add(f);
            f.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            作业 f = new 作业();
            f.TopLevel = false;
            panel4.Controls.Clear();
            panel4.Controls.Add(f);
            f.Show();
        }

        private void pictureBox11_Click(object sender, EventArgs e)
        {
            作业 f = new 作业();
            f.TopLevel = false;
            panel4.Controls.Clear();
            panel4.Controls.Add(f);
            f.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            账户 f = new 账户();
            f.TopLevel = false;
            panel4.Controls.Clear();
            panel4.Controls.Add(f);
            f.Show();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            学生设置 f = new 学生设置();
            f.TopLevel = false;
            panel4.Controls.Clear();
            panel4.Controls.Add(f);
            f.Show();
        }

        #region 退出提示
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("点击关闭后，程序会自动退出！", "！！！提示", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                e.Cancel = false;
                System.Environment.Exit(0);
                Application.Exit();
            }
            else
            {
                e.Cancel = true;
            }
        }
        #endregion

        private void button8_Click(object sender, EventArgs e)
        {
            游戏 f = new 游戏();
            f.TopLevel = false;
            panel4.Controls.Clear();
            panel4.Controls.Add(f);
            f.Show();
        }
    }
}
