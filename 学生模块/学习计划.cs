﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 系统应用开发
{
    public partial class 学习计划 : Form
    {
        Sql sql = new Sql();
        String name;

        int line;
        public 学习计划()
        {
            InitializeComponent();
            string startupPath = Application.StartupPath;
            startupPath = startupPath.Substring(0, startupPath.LastIndexOf("\\"));
            string path = startupPath.Substring(0, startupPath.LastIndexOf("\\"));
            this.BackgroundImage = Image.FromFile(path+@"\\图片素材\\学生4.jpg");
            this.BackgroundImageLayout = ImageLayout.Stretch;

            String s = "select * from [学生表]";
            DataSet ds = new DataSet();
            ds = sql.Query(s);
            DataTable datable = ds.Tables["table1"];
            dataGridView1.DataSource = datable;

            for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
            {
                for (int j = 0; j < dataGridView1.Columns.Count; j++)
                {
                    if (dataGridView1.Rows[i].Cells[j].Value.ToString().Trim() == 登录界面.TEXT.ToString())
                    {
                        name = dataGridView1.Rows[i].Cells[j + 2].Value.ToString().Trim();
                        break;
                    }
                }
            }

            s = "select * from [" + name + "的学习计划]";
            ds = sql.Query(s);
            datable = ds.Tables["table1"];
            dataGridView1.DataSource = datable;
        }

        #region 修改模块
        private void button1_Click(object sender, EventArgs e)
        {
            String s = "update [" + name + "的学习计划] set 周一放学后 = @text1,周二放学后 = @text2," +
                "周三放学后 = @text3,周四放学后 = @text4,周五放学后 = @text5,周六一天 = @text6,周日半天 = @text7"+
                " where 时间段编号 = @time";

            SqlParameter[] sqlParameters =
            {
                new SqlParameter("time",textBox8.Text.Trim()),
                new SqlParameter("text1",textBox1.Text.Trim()),
                new SqlParameter("text2",textBox2.Text.Trim()),
                new SqlParameter("text3",textBox3.Text.Trim()),
                new SqlParameter("text4",textBox4.Text.Trim()),
                new SqlParameter("text5",textBox5.Text.Trim()),
                new SqlParameter("text6",textBox6.Text.Trim()),
                new SqlParameter("text7",textBox7.Text.Trim()),
            };

            int flag = sql.NonQuery(s, sqlParameters);

            if (flag == 0)
            {
                MessageBox.Show("修改失败，请重新尝试！", "注意！！！");
            }
            else
            {
                MessageBox.Show("修改成功！");
                button1_attach();
                textBox1.Text = "";
                textBox2.Text = "";
                textBox3.Text = "";
                textBox4.Text = "";
                textBox5.Text = "";
                textBox6.Text = "";
                textBox7.Text = "";
                textBox8.Text = "";
            }
        }

        public void button1_attach()
        {
            String s = "select * from [" + name + "的学习计划]";
            DataTable table = new DataTable();
            DataSet ds = new DataSet();
            ds = sql.Query(s);
            DataTable datable = ds.Tables["table1"];
            dataGridView1.DataSource = datable;
        }
        #endregion

        #region 增加模块
        private void button2_Click(object sender, EventArgs e)
        {
            String s = "insert into [" + name + "的学习计划] values (@text,@text1,@text2,@text3,@text4,@text5,@text6,@text7)";

            SqlParameter[] sqlParameters =
            {
                new SqlParameter("text",textBox8.Text.Trim()),
                new SqlParameter("text1",textBox1.Text.Trim()),
                new SqlParameter("text2",textBox2.Text.Trim()),
                new SqlParameter("text3",textBox3.Text.Trim()),
                new SqlParameter("text4",textBox4.Text.Trim()),
                new SqlParameter("text5",textBox5.Text.Trim()),
                new SqlParameter("text6",textBox6.Text.Trim()),
                new SqlParameter("text7",textBox7.Text.Trim()),
            };

            int flag = 0;

            if (textBox1.Text == "" || textBox2.Text == "" || textBox3.Text == "" || textBox8.Text == ""
                || textBox4.Text == "" || textBox5.Text == "" || textBox6.Text == "" || textBox7.Text == "")
            {
                flag = 0;
            }
            else
            {
                flag = sql.NonQuery(s, sqlParameters);
            }

            if (flag == 0)
            {
                MessageBox.Show("增加失败，请重新尝试！");
            }
            else
            {
                MessageBox.Show("增加成功！");
                button1_attach();
                textBox1.Text = "";
                textBox2.Text = "";
                textBox3.Text = "";
                textBox4.Text = "";
                textBox5.Text = "";
                textBox6.Text = "";
                textBox7.Text = "";
                textBox8.Text = "";
            }
        }
        #endregion

        #region 删除模块
        private void button3_Click(object sender, EventArgs e)
        {
            String s = "delete from [" + name + "的学习计划] where 时间段编号 = @time";

            SqlParameter[] sqlParameters =
            {
                new SqlParameter("time",textBox8.Text.Trim()),
            };

            int flag = sql.NonQuery(s, sqlParameters);

            if (flag == 0)
            {
                MessageBox.Show("删除失败，请重新尝试！");
            }
            else
            {
                MessageBox.Show("删除成功！");
                button1_attach();
                textBox1.Text = "";
                textBox2.Text = "";
                textBox3.Text = "";
                textBox4.Text = "";
                textBox5.Text = "";
                textBox6.Text = "";
                textBox7.Text = "";
                textBox8.Text = "";
            }
        }
        #endregion

        #region 点击功能
        private void dataGridView1_Click(object sender, DataGridViewCellEventArgs e)
        {
            int line = e.RowIndex;
            if (line >= 0)
            {
                textBox1.Text = dataGridView1.Rows[line].Cells[1].Value.ToString();
                textBox2.Text = dataGridView1.Rows[line].Cells[2].Value.ToString();
                textBox3.Text = dataGridView1.Rows[line].Cells[3].Value.ToString();
                textBox4.Text = dataGridView1.Rows[line].Cells[4].Value.ToString();
                textBox5.Text = dataGridView1.Rows[line].Cells[5].Value.ToString();
                textBox6.Text = dataGridView1.Rows[line].Cells[6].Value.ToString();
                textBox7.Text = dataGridView1.Rows[line].Cells[7].Value.ToString();
                textBox8.Text = dataGridView1.Rows[line].Cells[0].Value.ToString();
            }
        }
        #endregion
    }
}
