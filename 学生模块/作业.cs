﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 系统应用开发
{
    public partial class 作业 : Form
    {
        Sql sql = new Sql();
        public 作业()
        {
            InitializeComponent();
            string startupPath = Application.StartupPath;
            startupPath = startupPath.Substring(0, startupPath.LastIndexOf("\\"));
            string path = startupPath.Substring(0, startupPath.LastIndexOf("\\"));
            this.BackgroundImage = Image.FromFile(path+@"\\图片素材\\学生6.jpg");
            this.BackgroundImageLayout = ImageLayout.Stretch;

            String s = "select * from [作业表]";
            DataSet ds = new DataSet();
            ds = sql.Query(s);
            DataTable datable = ds.Tables["table1"];
            dataGridView1.DataSource = datable;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            label2.Text = "今天是："+DateTime.Now.ToString()+"  "+DateTime.Now.DayOfWeek;
        }

        //点击功能
        private void dataGridView1_Click(object sender, DataGridViewCellEventArgs e)
        {
            int line = e.RowIndex;
            if (line >= 0)
            {
                textBox1.Text = dataGridView1.Rows[line].Cells[0].Value.ToString();
                textBox2.Text = dataGridView1.Rows[line].Cells[1].Value.ToString();
                textBox3.Text = dataGridView1.Rows[line].Cells[2].Value.ToString();
                textBox4.Text = dataGridView1.Rows[line].Cells[3].Value.ToString();
                textBox5.Text = dataGridView1.Rows[line].Cells[4].Value.ToString();
                textBox6.Text = dataGridView1.Rows[line].Cells[5].Value.ToString();
                textBox7.Text = dataGridView1.Rows[line].Cells[6].Value.ToString();
                textBox8.Text = dataGridView1.Rows[line].Cells[7].Value.ToString();
                textBox9.Text = dataGridView1.Rows[line].Cells[8].Value.ToString();
            }
        }
    }
}
