﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 系统应用开发
{
    public partial class 选择界面 : Form
    {
        public 选择界面()
        {
            InitializeComponent();

            string startupPath = Application.StartupPath;
            startupPath = startupPath.Substring(0, startupPath.LastIndexOf("\\"));
            string path = startupPath.Substring(0, startupPath.LastIndexOf("\\"));
            this.BackgroundImage = Image.FromFile(path + @"\\图片素材\\游戏4.jpg");
            this.BackgroundImageLayout = ImageLayout.Stretch;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            拼图游戏界面 form1 = new 拼图游戏界面(3);
            form1.Show();
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            拼图游戏界面 form1 = new 拼图游戏界面(4);
            form1.Show();
            this.Hide();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            拼图游戏界面 form1 = new 拼图游戏界面(5);
            form1.Show();
            this.Hide();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Dispose();
            拼图小游戏 form = new 拼图小游戏();
            form.Show();
        }

        #region 游戏退出提示
        private void Form3_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("点击关闭后，程序会自动退出！", "！！！提示", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                e.Cancel = false;
                this.Dispose();
            }
            else
            {
                e.Cancel = true;
            }
        }
        #endregion
    }
}
