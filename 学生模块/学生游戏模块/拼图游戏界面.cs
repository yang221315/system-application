﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 系统应用开发
{
    public partial class 拼图游戏界面 : Form
    {
        #region 返回上一个界面
        private void button3_Click(object sender, EventArgs e)
        {
            this.Dispose();
            选择界面 form = new 选择界面();
            form.Show();
        }
        #endregion

        #region 点击退出游戏按钮，自动退出
        private void button2_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
        #endregion

        #region 生成按钮数组

        Function func;
        //int N;
        //Button[,] buttons;
        //int height = 50;
        //int width = 50;
        public 拼图游戏界面(int n)
        {
            //this.N = n;
            func = new Function(n);
            //buttons = new Button[N, N];
            InitializeComponent();
            string startupPath = Application.StartupPath;
            startupPath = startupPath.Substring(0, startupPath.LastIndexOf("\\"));
            string path = startupPath.Substring(0, startupPath.LastIndexOf("\\"));
            this.BackgroundImage = Image.FromFile(path + @"\\图片素材\\游戏2.jpg");
            this.BackgroundImageLayout = ImageLayout.Stretch;
        }

        //使用两个for循环嵌套实现产生N*N个button
        private void Form1_Load(object sender, EventArgs e)
        {
            for (int k = 0; k < func.Buttons.GetLength(0); k++)
            {
                for (int i = 0; i < func.Buttons.GetLength(1); i++)
                {
                    Button btn = new Button();
                    btn.Click += func.button_Click;
                    btn.Tag = k * func.Buttons.GetLength(0) + i;
                    btn.Text = (i + k * func.N1 + 1).ToString();
                    btn.Height = func.Height;
                    btn.Width = func.Width;
                    btn.Left = 300 + i * 60;
                    btn.Top = 30 + 60 * k;
                    this.Controls.Add(btn);
                    func.Buttons[k, i] = btn;
                }
            }
            //最后的button不可见
            func.Buttons[func.N1 - 1, func.N1 - 1].Visible = false;
        }
        #endregion

        #region 完成游戏功能
        //实现开始游戏按钮的功能，打乱顺序
        private void button1_Click(object sender, EventArgs e)
        {
            Random rnd1 = new Random();
            Random rnd2 = new Random();
            Random rnd3 = new Random();
            Random rnd4 = new Random();
            int num1, num2, num3, num4;
            for (int i = 0; i < 1000; i++)
            {
                num1 = rnd1.Next(0, func.N1);
                num2 = rnd2.Next(0, 300) % func.N1;
                num3 = rnd3.Next(10, 80) % func.N1;
                num4 = rnd4.Next(5, 30) % func.N1;
                if (func.Buttons[num1, num2].Visible == false || func.Buttons[num3, num4].Visible == false)
                {
                    func.swap(func.Buttons[num1, num2], func.Buttons[num3, num4]);
                }
            }
        }
        #endregion

        #region 游戏退出提示
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("点击关闭后，程序会自动退出！", "！！！提示", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                e.Cancel = false;
                this.Dispose();
            }
            else
            {
                e.Cancel = true;
            }
        }
        #endregion
    }

    class Function
    {
        private Button[,] buttons;
        private int height = 50;
        private int width = 50;
        private int N;

        public Function(int n)
        {
            N1 = n;
            Buttons = new Button[n, n];
        }

        public Button[,] Buttons
        {
            get => buttons;
            set => buttons = value;
        }
        public int Height
        {
            get => height;
            set => height = value;
        }
        public int Width
        {
            get => width;
            set => width = value;
        }
        public int N1
        {
            get => N;
            set => N = value;
        }

        #region 按钮控件设置
        public void button_Click(object sender, EventArgs e)
        {
            Button button = sender as Button;
            Button btn = FindBlank();

            bool b = IsNeigbour(button, btn);

            //判断是否交换两个button
            if (b)
            {
                swap(button, btn);
            }

            int flag = Isright(buttons);

            //判断游戏是否结束
            if (flag == 0)
            {
                MessageBox.Show("游戏结束");
            }
        }
        #endregion

        #region 寻找空白块
        //寻找空白块
        public Button FindBlank()
        {
            //使用两个for循环遍历查找空白块
            Button button = new Button();
            for (int i = 0; i < buttons.GetLength(0); i++)
            {
                for (int j = 0; j < buttons.GetLength(1); j++)
                {
                    if (buttons[i, j].Visible == false)
                    {
                        button = buttons[i, j];
                    }
                }
            }
            return button;
        }
        #endregion

        #region 判断是否相邻
        //判断是否与空白块相邻
        public bool IsNeigbour(Button button, Button btn)
        {
            int line, cow;
            int line1, cow1;
            line = (int)button.Tag / buttons.GetLength(0);
            cow = (int)button.Tag % buttons.GetLength(0);
            line1 = (int)btn.Tag / buttons.GetLength(0);
            cow1 = (int)btn.Tag % buttons.GetLength(0);

            if (line == line1)
            {
                if (cow - cow1 == 1 || cow - cow1 == -1)
                {
                    return true;
                }

                else
                {
                    return false;
                }
            }

            else if (cow == cow1)
            {
                if (line - line1 == 1 || line - line1 == -1)
                {
                    return true;
                }

                else
                {
                    return false;
                }
            }

            else
            {
                return false;
            }
        }
        #endregion

        #region 游戏结束判断
        //游戏结束判断
        public int Isright(Button[,] btns)
        {
            int flag = 0;
            int k = 1;
            for (int i = 0; i < btns.GetLength(0); i++)
            {
                for (int j = 0; j < btns.GetLength(1); j++)
                {
                    if (btns[i, j].Text.Equals(k.ToString()) == false)
                    {
                        flag = 1;
                    }
                    k++;
                }
            }
            return flag;
        }
        #endregion

        #region 交换两个button的相关属性
        //交换功能
        public void swap(Button btn1, Button btn2)
        {
            //交换文本属性
            string text = "";
            text = btn1.Text;
            btn1.Text = btn2.Text;
            btn2.Text = text;

            //交换可见属性
            bool vis;
            vis = btn1.Visible;
            btn1.Visible = btn2.Visible;
            btn2.Visible = vis;
        }
        #endregion
    }
}
