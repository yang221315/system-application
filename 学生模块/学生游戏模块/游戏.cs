﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 系统应用开发
{
    public partial class 游戏 : Form
    {
        public 游戏()
        {
            InitializeComponent();
            string startupPath = Application.StartupPath;
            startupPath = startupPath.Substring(0, startupPath.LastIndexOf("\\"));
            string path = startupPath.Substring(0, startupPath.LastIndexOf("\\"));
            this.BackgroundImage = Image.FromFile(path + @"\\图片素材\\游戏5.jpg");
            this.BackgroundImageLayout = ImageLayout.Stretch;
        }

        //数字小游戏
        private void button1_Click(object sender, EventArgs e)
        {
            数字小游戏 f = new 数字小游戏();
            f.Show();
        }

        //拼图小游戏
        private void button2_Click(object sender, EventArgs e)
        {
            拼图小游戏 f = new 拼图小游戏();
            f.Show();
        }

        //网络小游戏
        private void button3_Click(object sender, EventArgs e)
        {
            Help.ShowHelp(this, "http://www.4399.com");
        }
    }
}
