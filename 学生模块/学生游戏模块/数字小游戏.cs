﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 系统应用开发
{
    public partial class 数字小游戏 : Form
    {
        Suanshu suanshu;
        private int flag;
        private string s = "";

        public 数字小游戏()
        {
            InitializeComponent();
            string startupPath = Application.StartupPath;
            startupPath = startupPath.Substring(0, startupPath.LastIndexOf("\\"));
            string path = startupPath.Substring(0, startupPath.LastIndexOf("\\"));
            this.BackgroundImage = Image.FromFile(path + @"\\图片素材\\游戏3.jpg");
            this.BackgroundImageLayout = ImageLayout.Stretch;
        }

        //出题
        private void button1_Click(object sender, EventArgs e)
        {
            suanshu = new Suanshu();
            textBox1.Text = suanshu.Num1.ToString() + "  " + suanshu.Op + "  " + suanshu.Num2.ToString() + "  " + '=';
        }

        private void button2_Click(object sender, EventArgs e)
        {
            suanshu.Compute();
            if (Convert.ToInt32(textBox2.Text) == suanshu.Result)
            {
                flag = 1;
            }
            else
            {
                flag = 0;
            }

            if (flag == 1)
            {
                MessageBox.Show("答案正确");
                s = "正确";
            }
            else
            {
                MessageBox.Show("答案错误");
                s = "错误";
            }

            textBox3.Text = textBox3.Text + suanshu.Num1.ToString() + "  " +
                suanshu.Op + "  " + suanshu.Num2.ToString() + "  " + '=' +
                "  " + textBox2.Text + "  " + s + "\r\n";
        }
    }
    class Suanshu
    {
        private int num1, num2, num3, result;
        private char op;

        public int Num1 { get => num1; set => num1 = value; }
        public int Num2 { get => num2; set => num2 = value; }
        public int Num3 { get => num3; set => num3 = value; }
        public int Result { get => result; set => result = value; }
        public char Op { get => op; set => op = value; }

        public Suanshu()
        {
            Random rnd = new Random();
            this.Num1 = rnd.Next(100, 1000);
            this.Num2 = rnd.Next(100, 1000);
            this.Num3 = rnd.Next(0, 4);
            switch (this.Num3)
            {
                case 0:
                    Op = '+';
                    break;
                case 1:
                    Op = '-';
                    break;
                case 2:
                    Op = '*';
                    break;
                case 3:
                    Op = '/';
                    break;
                default:
                    Op = '?';
                    break;
            }
        }

        public void Compute()
        {
            if (Op == '+')
            {
                Result = Num1 + Num2;
            }

            if (Op == '-')
            {
                Result = Num1 - Num2;
            }

            if (Op == '*')
            {
                Result = Num1 * Num2;
            }

            if (Op == '/')
            {
                Result = Num1 / Num2;
            }
        }
    }
}
