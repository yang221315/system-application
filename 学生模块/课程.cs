﻿using System;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;

namespace 系统应用开发
{
    public partial class 课程 : Form
    {
        Sql sql = new Sql();
        String name;

        int line, lie;
        public 课程()
        {
            InitializeComponent();
            string startupPath = Application.StartupPath;
            startupPath = startupPath.Substring(0, startupPath.LastIndexOf("\\"));
            string path = startupPath.Substring(0, startupPath.LastIndexOf("\\"));
            this.BackgroundImage = Image.FromFile(path+@"\\图片素材\\学生1.jpg");
            this.BackgroundImageLayout = ImageLayout.Stretch;

            String s = "select * from [学生表]";
            DataSet ds = new DataSet();
            ds = sql.Query(s);
            DataTable datable = ds.Tables["table1"];
            dataGridView1.DataSource = datable;

            for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
            {
                for (int j = 0; j < dataGridView1.Columns.Count; j++)
                {
                    if (dataGridView1.Rows[i].Cells[j].Value.ToString().Trim() == 登录界面.TEXT.ToString())
                    {
                        name = dataGridView1.Rows[i].Cells[j + 2].Value.ToString().Trim();
                        break;
                    }
                }
            }

            s = "select * from [" + name + "的课程表]";
            ds = sql.Query(s);
            datable = ds.Tables["table1"];
            dataGridView1.DataSource = datable;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            int flag = 0;

            if (lie == 1)
            {
                String s = "update [" + name + "的课程表] set 星期一 = @text2 where 课程安排 = @text1";

                SqlParameter[] sqlParameters =
                {
                    new SqlParameter("text1",dataGridView1.Rows[line].Cells[lie-1].Value.ToString().Trim()),
                    new SqlParameter("text2",textBox1.Text.Trim()),
                };

                flag = sql.NonQuery(s, sqlParameters);
            }

            if (lie == 2)
            {
                String s = "update [" + name + "的课程表] set 星期二 = @text2 where 课程安排 = @text1";

                SqlParameter[] sqlParameters =
                {
                    new SqlParameter("text1",dataGridView1.Rows[line].Cells[lie-2].Value.ToString().Trim()),
                    new SqlParameter("text2",textBox1.Text.Trim()),
                };

                flag = sql.NonQuery(s, sqlParameters);
            }

            if (lie == 3)
            {
                String s = "update [" + name + "的课程表] set 星期三 = @text2 where 课程安排 = @text1";

                SqlParameter[] sqlParameters =
                {
                    new SqlParameter("text1",dataGridView1.Rows[line].Cells[lie-3].Value.ToString().Trim()),
                    new SqlParameter("text2",textBox1.Text.Trim()),
                };

                flag = sql.NonQuery(s, sqlParameters);
            }

            if (lie == 4)
            {
                String s = "update [" + name + "的课程表] set 星期四 = @text2 where 课程安排 = @text1";

                SqlParameter[] sqlParameters =
                {
                    new SqlParameter("text1",dataGridView1.Rows[line].Cells[lie-4].Value.ToString().Trim()),
                    new SqlParameter("text2",textBox1.Text.Trim()),
                };

                flag = sql.NonQuery(s, sqlParameters);
            }

            if (lie == 5)
            {
                String s = "update [" + name + "的课程表] set 星期五 = @text2 where 课程安排 = @text1";

                SqlParameter[] sqlParameters =
                {
                    new SqlParameter("text1",dataGridView1.Rows[line].Cells[lie-5].Value.ToString().Trim()),
                    new SqlParameter("text2",textBox1.Text.Trim()),
                };

                flag = sql.NonQuery(s, sqlParameters);
            }
            if (lie == 6)
            {
                String s = "update [" + name + "的课程表] set 星期六 = @text2 where 课程安排 = @text1";

                SqlParameter[] sqlParameters =
                {
                    new SqlParameter("text1",dataGridView1.Rows[line].Cells[lie-6].Value.ToString().Trim()),
                    new SqlParameter("text2",textBox1.Text.Trim()),
                };

                flag = sql.NonQuery(s, sqlParameters);
            }

            if (lie == 7)
            {
                String s = "update [" + name + "的课程表] set 星期天 = @text2 where 课程安排 = @text1";

                SqlParameter[] sqlParameters =
                {
                    new SqlParameter("text1",dataGridView1.Rows[line].Cells[lie-7].Value.ToString().Trim()),
                    new SqlParameter("text2",textBox1.Text.Trim()),
                };

                flag = sql.NonQuery(s, sqlParameters);
            }


            if (flag == 0)
            {
                MessageBox.Show("修改失败，请重新尝试！", "注意！！！");
            }
            else
            {
                MessageBox.Show("修改成功！");
                button1_attach();
                textBox1.Text = "";
            }
        }

        public void button1_attach()
        {
            String s = "select * from [" + name + "的课程表]";
            DataTable table = new DataTable();
            DataSet ds = new DataSet();
            ds = sql.Query(s);
            DataTable datable = ds.Tables["table1"];
            dataGridView1.DataSource = datable;
        }

        private void dataGridView1_Click(object sender, DataGridViewCellEventArgs e)
        {
            line = e.RowIndex;
            lie = e.ColumnIndex;
            if (line >= 0)
            {
                textBox1.Text = dataGridView1.Rows[line].Cells[lie].Value.ToString().Trim();
            }
        }
    }
}
