﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 系统应用开发
{
    public partial class 目标 : Form
    {
        public 目标()
        {
            InitializeComponent();
            string startupPath = Application.StartupPath;
            startupPath = startupPath.Substring(0, startupPath.LastIndexOf("\\"));
            string path = startupPath.Substring(0, startupPath.LastIndexOf("\\"));
            this.BackgroundImage = Image.FromFile(path+@"\\图片素材\\学生2.jpg");
            this.BackgroundImageLayout = ImageLayout.Stretch;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            label1.Text = DateTime.Now.ToString("yyyy") + "年未实现的目标";
            label2.Text = DateTime.Now.ToString("yyyy") + "年已实现的目标";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string s = "";

            for(int i=0;i<listBox1.Items.Count;i++)
            {
                if(textBox2.Text == listBox1.Items[i].ToString())
                {
                    s = listBox1.Items[i].ToString();
                    break;
                }
            }

            listBox1.Items.Remove(s);

            listBox2.Items.Add(s);

            textBox2.Text = "";
        }

        private void button5_Click(object sender, EventArgs e)
        {
            string s = "";

            for (int i = 0; i < listBox2.Items.Count; i++)
            {
                if (textBox2.Text == listBox2.Items[i].ToString())
                {
                    s = listBox2.Items[i].ToString();
                    break;
                }
            }

            listBox2.Items.Remove(s);

            listBox1.Items.Add(s);

            textBox2.Text = "";
        }

        //写入txt文件
        private int WriteListToTxt()
        {
            int flag = 0, flag1 = 0;

            string startupPath = Application.StartupPath;
            startupPath = startupPath.Substring(0, startupPath.LastIndexOf("\\"));
            string path1 = startupPath.Substring(0, startupPath.LastIndexOf("\\"));

            String path = path1+@"\\愿望清单\\" + 登录界面.TEXT + "未完成.txt";
            try
            {
                int count = listBox1.Items.Count;
                StreamWriter _wstream = new StreamWriter(path);
                for (int i = 0; i < count; i++)
                {
                    string data = listBox1.Items[i].ToString();
                    _wstream.Write(data);
                    _wstream.WriteLine();
                }
                _wstream.Flush();
                _wstream.Close();
            }
            catch(Exception e)
            {
                MessageBox.Show("无法写入数据");
                flag = 1;
            }


            String path2 = path1+@"\\愿望清单\\" + 登录界面.TEXT + "已完成.txt";
            try
            {
                int count2 = listBox2.Items.Count;
                StreamWriter _wstream2 = new StreamWriter(path2);
                for (int i = 0; i < count2; i++)
                {
                    string data = listBox2.Items[i].ToString();
                    _wstream2.Write(data);
                    _wstream2.WriteLine();
                }
                _wstream2.Flush();
                _wstream2.Close();
            }
            catch(Exception e)
            {
                MessageBox.Show("无法写入数据");
                flag1 = 1;
            }

            if (flag == 1 || flag1 == 1)
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }

        //从txt文件中读取数据
        private int ReadTxtToList()
        {
            int flag = 0,flag1 = 0;

            string startupPath = Application.StartupPath;
            startupPath = startupPath.Substring(0, startupPath.LastIndexOf("\\"));
            string path1 = startupPath.Substring(0, startupPath.LastIndexOf("\\"));
            String path = path1+@"\\愿望清单\\" + 登录界面.TEXT + "未完成.txt";
            try
            {
                StreamReader _rstream = new StreamReader(path, System.Text.Encoding.UTF8);
                string line;
                while ((line = _rstream.ReadLine()) != null)
                {
                    listBox1.Items.Add(line);
                }
                _rstream.Close();
            }
            catch(Exception e)
            {
                MessageBox.Show("无法读到数据1");
                flag = 1;
            }


            String path2 = path1+@"\\愿望清单\\" + 登录界面.TEXT + "已完成.txt";
            try
            {
                StreamReader _rstream2 = new StreamReader(path2, System.Text.Encoding.UTF8);
                string line2;
                while ((line2 = _rstream2.ReadLine()) != null)
                {
                    listBox2.Items.Add(line2);
                }
                _rstream2.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show("无法读到数据2");
                flag1 = 1;
            }

            if(flag==1 || flag1==1)
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            int result = WriteListToTxt();
            if(result==1)
                MessageBox.Show("数据导入成功");
        }

        private void button7_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            listBox2.Items.Clear();
            int result = ReadTxtToList();
            if(result==1)
            {
                MessageBox.Show("数据导出成功");
            }
        }

        //增加目标
        private void button1_Click(object sender, EventArgs e)
        {
            listBox1.Items.Add(textBox1.Text);
            textBox1.Text = "";
        }

        //删除目标
        private void button2_Click(object sender, EventArgs e)
        {
            listBox1.Items.Remove(textBox1.Text);
            textBox1.Text = "";
        }

        //修改目标
        private void button3_Click(object sender, EventArgs e)
        {
            listBox1.Items.Remove(textBox1.Text);
            listBox1.Items.Add(textBox3.Text);
            textBox1.Text = "";
            textBox3.Text = "";
        }
    }
}
